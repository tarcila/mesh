get_filename_component(_current_directory ${CMAKE_CURRENT_LIST_FILE} PATH)

include(${_current_directory}/Policies.cmake NO_POLICY_SCOPE)
include(${_current_directory}/DefaultCompilerFlags.cmake)
include(${_current_directory}/DefaultLayout.cmake)
include(${_current_directory}/DefaultNaming.cmake)
include(${_current_directory}/Projects.cmake)
include(${_current_directory}/DeployDependency.cmake)
