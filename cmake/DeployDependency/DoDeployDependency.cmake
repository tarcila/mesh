include(GetPrerequisites)
get_filename_component(TARGET_DIR "${TARGET}" DIRECTORY)
get_prerequisites("${TARGET}" PREREQUISITES 1 1 "${TARGET}" "${DIRS}")

message(STATUS "Deploying prerequisites ${PREREQUISITES}")

foreach(pre ${PREREQUISITES})
  foreach(dir ${DIRS})
    if(EXISTS "${dir}/${pre}")
	  execute_process(COMMAND ${CMAKE_COMMAND} -E copy_if_different ${dir}/${pre} ${TARGET_DIR}/${pre})
	  break()
	endif()
  endforeach()
endforeach()
