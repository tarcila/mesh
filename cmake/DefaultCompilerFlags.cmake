# mandatory flags
set_property(DIRECTORY ${CMAKE_SOURCE_DIR} APPEND PROPERTY COMPILE_DEFINITIONS "_LARGEFILE_SOURCE" "_FILE_OFFSET_BITS=64")
define_property(CACHED_VARIABLE PROPERTY CMAKE_ADDITIONAL_C_FLAGS BRIEF_DOCS "C flags used by the compiler during all build types." FULL_DOCS "C flags used by the compiler during all build types.")
define_property(CACHED_VARIABLE PROPERTY CMAKE_ADDITIONAL_CXX_FLAGS BRIEF_DOCS "CXX flags used by the compiler during all build types." FULL_DOCS "CXX flags used by the compiler during all build types.")
mark_as_advanced(CMAKE_ADDITIONAL_C_FLAGS CMAKE_ADDITIONAL_CXX_FLAGS)

if(CMAKE_COMPILER_IS_GNUCC)
  set(CMAKE_ADDITIONAL_C_FLAGS "-Wall -W")
  set(CMAKE_ADDITIONAL_CXX_FLAGS "-Wall -W")
  if(CMAKE_VERBOSE_MAKEFILE)
    # force gcc one line message generation : helps some tools (eclipse...) to parse error messages
    # only activated in verbose mode because versbose mode is usually also required
    set(CMAKE_ADDITIONAL_C_FLAGS "${CMAKE_ADDITIONAL_C_FLAGS} -fmessage-length=0")
    set(CMAKE_ADDITIONAL_CXX_FLAGS "${CMAKE_ADDITIONAL_CXX_FLAGS} -fmessage-length=0")
  endif()

  # c++14 support
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")

  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wa,--noexecstack -fstack-protector --param=ssp-buffer-size=4")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wa,--noexecstack -fstack-protector --param=ssp-buffer-size=4")
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE}  -D_FORTIFY_SOURCE=2")
  set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} -D_FORTIFY_SOURCE=2")
  set(CMAKE_CXX_FLAGS_MINSIZEREL "${CMAKE_CXX_FLAGS_MINSIZEREL} -D_FORTIFY_SOURCE=2")
elseif(MSVC)
  set(CMAKE_C_WARNING_LEVEL "4" CACHE STRING "compiler warning level for CPP")
  set(CMAKE_CXX_WARNING_LEVEL "4" CACHE STRING "compiler warning level for CPP")
  mark_as_advanced(CMAKE_CXX_WARNING_LEVEL CMAKE_C_WARNING_LEVEL CMAKE_MSVC_DISABLE_WARNING)

  foreach(warning ${CMAKE_MSVC_DISABLE_WARNING})
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /wd${warning}")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd${warning}")
  endforeach()
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /D_SCL_SECURE_NO_WARNINGS /D_CRT_SECURE_NO_WARNINGS")
endif()

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${CMAKE_ADDITIONAL_C_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CMAKE_ADDITIONAL_CXX_FLAGS}")
