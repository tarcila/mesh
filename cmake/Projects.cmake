# Solution and project handling
# Solution uses folders
set_property(GLOBAL PROPERTY USE_FOLDERS TRUE)
# projects can be reorganized using filesystem hierarchy
set(SOURCE_GROUP_DELIMITER "/")

function(set_source_groups)
  source_group("Source Files" REGULAR_EXPRESSION ".*\\.(c|cxx|cpp|cu)")
endfunction()

function(add_executable)
  set_source_groups()
  _add_executable(${ARGN})
endfunction()

function(add_library)
  set_source_groups()
  _add_library(${ARGN})
endfunction()