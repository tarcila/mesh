if(DEPLOY_DEPENDENCY_ALREADY_INCLUDED)
  return()
endif()
set(DEPLOY_DEPENDENCY_ALREADY_INCLUDED TRUE)

define_property(
	TARGET
	PROPERTY DEPLOY_DEPENDENCY_SOURCE_DIRS INHERITED
	BRIEF_DOCS "List of directories to search for dependency for given target."
	FULL_DOCS "Corresponding to the dirs parameter of fixup_bundle (BundleUtilities.cmake)."
)
get_filename_component(DEPLOY_DEPENDENCY_CMAKE_DIRECTORY ${CMAKE_CURRENT_LIST_FILE} PATH)

function(target_deploy_dependencies TARGET)
  get_property(has_dirs TARGET ${TARGET} PROPERTY DEPLOY_DEPENDENCY_SOURCE_DIRS SET)
  if(has_dirs)
    get_property(dirs TARGET ${TARGET} PROPERTY DEPLOY_DEPENDENCY_SOURCE_DIRS)
    add_custom_command(TARGET ${TARGET} POST_BUILD
		COMMAND cmake "-DTARGET=$<TARGET_FILE:${TARGET}>" "-DDIRS=${dirs}" -P "${DEPLOY_DEPENDENCY_CMAKE_DIRECTORY}/DeployDependency/DoDeployDependency.cmake" VERBATIM)
  endif()
endfunction()

