#include "SkinExtractTBB.hpp"

#include <vtkDataSetSurfaceFilter.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLUnstructuredGridReader.h>
#include <vtkXMLPolyDataWriter.h>

#include <chrono>

namespace stdc = std::chrono;

int main(int argc, char** argv) {
    if (argc != 2 && argc != 3) {
        std::cerr << "syntax: SkinExtract <input.vtu> [<output.vtp>]" << std::endl;
        return 1;
    }

    std::string input(argv[1]);
    std::string output;
    if (argc == 3)
        output = argv[2];
    else
        output = input.substr(0, input.find_last_of(".")) + "-skin.vtp";

    std::cout << "skin extraction from " << input << " to " << output << std::endl;

    auto reader = vtkSmartPointer<vtkXMLUnstructuredGridReader>::New();
    reader->SetFileName(input.c_str());
    reader->Update();

    auto inputMesh = reader->GetOutput();

    auto surfaceFilter = vtkSmartPointer<vtkDataSetSurfaceFilter>::New();

    std::cout << "Starting VTK Extraction" << std::endl;
    auto startTime = stdc::high_resolution_clock::now();
    surfaceFilter->SetInputData(inputMesh);
    surfaceFilter->Update();
    auto skin = surfaceFilter->GetOutput();
    auto endTime = stdc::high_resolution_clock::now();
    std::cout << "VTK extraction " << stdc::duration<double>(endTime - startTime).count() << std::endl;
    std::cout << "Starting My Extraction" << std::endl;
    startTime = stdc::high_resolution_clock::now();
    auto skin2 = vtkSmartPointer<vtkPolyData>::Take(extractSkinTBB(inputMesh, ParallelExtraction::SEQUENTIAL));
    endTime = stdc::high_resolution_clock::now();
    std::cout << "My extraction " << stdc::duration<double>(endTime - startTime).count() << std::endl;
    std::cout << "Starting My Extraction //" << std::endl;
    startTime = stdc::high_resolution_clock::now();
    skin2 = vtkSmartPointer<vtkPolyData>::Take(extractSkinTBB(inputMesh, ParallelExtraction::PARALLEL));
    endTime = stdc::high_resolution_clock::now();
    std::cout << "My extraction // " << stdc::duration<double>(endTime - startTime).count() << std::endl;

    auto writer = vtkSmartPointer<vtkXMLPolyDataWriter>::New();
    writer->SetFileName(output.c_str());
    writer->SetInputData(skin2);
    writer->Write();

    return 0;
}
