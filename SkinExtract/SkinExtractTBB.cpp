#include "SkinExtractTBB.hpp"

#include <vtkGenericCell.h>
#include <vtkCellArray.h>
#include <vtkDataArray.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkUnsignedCharArray.h>
#include <vtkUnstructuredGrid.h>

#include <algorithm>
#include <iostream>
#include <memory>
#include <unordered_set>
#include <utility>

#include <tbb/enumerable_thread_specific.h>
#include <tbb/parallel_reduce.h>
#include <tbb/blocked_range.h>

using Face = std::shared_ptr<std::vector<vtkIdType>>;

struct HashFace {
    std::size_t operator()(const Face& face) const {
        return face->front();
    }
};

struct CompareFace {
    static tbb::enumerable_thread_specific<Face::element_type> commonFace;
    bool operator()(const Face& a, const Face& b) const {
        auto asize = a->size();
        if (asize != b->size() || ((*a)[0] != (*b)[0]))
            return false;

        switch (asize) {
        case 3: {
            return (((*a)[1] == (*b)[1] && (*a)[2] == (*b)[2]) ||
                ((*a)[1] == (*b)[2] && (*a)[2] == (*b)[1]));
            break;
        }
        case 4: {
            return (
                ((*a)[1] == (*b)[1] && (*a)[2] == (*b)[2] && (*a)[3] == (*b)[3]) ||  // 123 123
                ((*a)[1] == (*b)[1] && (*a)[3] == (*b)[2] && (*a)[2] == (*b)[3]) ||  // 132 123
                ((*a)[2] == (*b)[1] && (*a)[1] == (*b)[2] && (*a)[3] == (*b)[3]) ||  // 213 123
                ((*a)[3] == (*b)[1] && (*a)[1] == (*b)[2] && (*a)[2] == (*b)[3]) ||  // 312 123
                ((*a)[2] == (*b)[1] && (*a)[3] == (*b)[2] && (*a)[1] == (*b)[3]) ||  // 231 123
                ((*a)[3] == (*b)[1] && (*a)[2] == (*b)[2] && (*a)[1] == (*b)[3])    // 321 123
                );
            break;
        }
        default: {
            auto& f(commonFace.local());

            f.assign(a->cbegin(), a->cend());
            f.insert(f.end(), b->cbegin(), b->cend());
            std::sort(f.begin(), f.end());
            f.erase(std::unique(f.begin(), f.end()), f.end());
            return f.size() == asize;
            break;
        }
        }
    }
};
tbb::enumerable_thread_specific<Face::element_type> CompareFace::commonFace;

struct ParallelSkinExtract {
    struct GenericCell {
        vtkSmartPointer<vtkCell> cell[VTK_NUMBER_OF_CELL_TYPES];

        GenericCell()
        {
            for (int i = 0; i < VTK_NUMBER_OF_CELL_TYPES; ++i)
                cell[i] = vtkSmartPointer<vtkCell>::Take(vtkGenericCell::InstantiateCell(i));
        }
        GenericCell(const GenericCell& other) = delete;
        GenericCell& operator=(const GenericCell& other) = delete;
    };

    std::unordered_set<Face, HashFace, CompareFace> uniqueFaces;
    vtkCellArray* cells;
    vtkIdTypeArray* cellLocations;
    vtkUnsignedCharArray* cellTypes;
    static tbb::enumerable_thread_specific<GenericCell> tlsCell;
    static tbb::enumerable_thread_specific<std::vector<Face>> tlsFacePool;

    ParallelSkinExtract(vtkUnstructuredGrid* mesh)
        : cells(mesh->GetCells()),
        cellLocations(mesh->GetCellLocationsArray()),
        cellTypes(mesh->GetCellTypesArray()) {
        uniqueFaces.rehash(mesh->GetNumberOfPoints());
    }

    ParallelSkinExtract(ParallelSkinExtract& pse, tbb::split)
        : cells(pse.cells),
        cellLocations(pse.cellLocations),
        cellTypes(pse.cellTypes) {
        uniqueFaces.rehash(pse.uniqueFaces.bucket_count());
    }

    void operator()(const tbb::blocked_range<int>& r) {
        auto& facePool = tlsFacePool.local();
        auto& genericCell = tlsCell.local();
        for (auto i = r.begin(); i < r.end(); ++i) {
            auto cell = genericCell.cell[cellTypes->GetValue(i)];
            if (cell->GetCellDimension() != 3)
                continue;
            cell->GetPointIds()->SetNumberOfIds(0);
            cells->GetCell(cellLocations->GetValue(i), cell->GetPointIds());
            for (auto faceId = 0; faceId < cell->GetNumberOfFaces(); ++faceId) {
                auto faceCell = cell->GetFace(faceId);
                auto pointIds = faceCell->GetPointIds();
                Face face;
                if (facePool.empty())
                    face.reset(new Face::element_type);
                else {
                    face = facePool.back();
                    facePool.pop_back();
                }
                face->assign(pointIds->GetPointer(0), pointIds->GetPointer(0) + pointIds->GetNumberOfIds());
                std::rotate(face->begin(), std::min_element(face->begin(), face->end()), face->end());

                auto insert = uniqueFaces.insert(face);
                if (!insert.second) {
                    facePool.push_back(face);
                    facePool.push_back(*insert.first);
                    uniqueFaces.erase(insert.first);
                }
            }
        }
    }

    void join(ParallelSkinExtract& rhs) {
        for (const auto& face : rhs.uniqueFaces) {
            auto insert = uniqueFaces.insert(face);
            if (!insert.second)
                uniqueFaces.erase(insert.first);
        }
    }
};
tbb::enumerable_thread_specific<ParallelSkinExtract::GenericCell> ParallelSkinExtract::tlsCell;
tbb::enumerable_thread_specific<std::vector<Face>> ParallelSkinExtract::tlsFacePool;

vtkPolyData* extractSkinTBB(vtkUnstructuredGrid* mesh, ParallelExtraction doParallel) {
    ParallelSkinExtract pse(mesh);
    switch (doParallel) {
    case ParallelExtraction::PARALLEL: {
        tbb::parallel_reduce(tbb::blocked_range<int>(0, mesh->GetNumberOfCells(), 16384), pse);
        break;
    }
    case ParallelExtraction::SEQUENTIAL: {
        pse(tbb::blocked_range<int>(0, mesh->GetNumberOfCells()));
        break;
    }
    }

    const auto& uniqueFaces = pse.uniqueFaces;
    auto cells = vtkSmartPointer<vtkCellArray>::New();

    cells->Allocate(uniqueFaces.size());

    for (const auto& face : uniqueFaces) {
        cells->InsertNextCell(face->size(), face->data());
    }

    cells->Squeeze();

    auto skin = vtkPolyData::New();
    skin->SetPoints(mesh->GetPoints());
    skin->SetPolys(cells);

    return skin;
}
