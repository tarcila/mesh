#pragma once

class vtkUnstructuredGrid;
class vtkPolyData;


enum class ParallelExtraction {
    SEQUENTIAL,
    PARALLEL
};

vtkPolyData* extractSkinTBB(vtkUnstructuredGrid* mesh, ParallelExtraction doParallel = ParallelExtraction::PARALLEL);

