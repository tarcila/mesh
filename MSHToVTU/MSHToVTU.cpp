#include <vtkCellArray.h>
#include <vtkIntArray.h>
#include <vtkPoints.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLUnstructuredGridWriter.h>

#include <fstream>
#include <map>
#include <sstream>
#include <string>
#include <utility>

const std::map<int, std::tuple<vtkIdType, int> > typeMshToVtk = {
    { 1, std::make_tuple(VTK_LINE, 2) }, // 2-node line
    { 2, std::make_tuple(VTK_TRIANGLE, 3) }, // 3-node triangle
    { 3, std::make_tuple(VTK_QUAD, 4) }, // 4-node quadrangle
    { 4, std::make_tuple(VTK_TETRA, 4) }, // 4-node tetrahedron
    { 5, std::make_tuple(VTK_HEXAHEDRON, 8) }, // 8-node hexahedron
    { 6, std::make_tuple(VTK_WEDGE, 6) }, // 6-node prism
    { 7, std::make_tuple(VTK_PYRAMID, 5) }, // 5-node pyramid
    { 15, std::make_tuple(VTK_VERTEX, 1) }
};


vtkUnstructuredGrid* readMSH(const std::string& filepath) {
    enum {
        UNDEFINED,
        MESH_FORMAT,
        NODES,
        ELEMENTS
    } state = UNDEFINED;

    std::ifstream ifs(filepath);
    if (!ifs.is_open())
        return nullptr;

    auto cells = vtkSmartPointer<vtkCellArray>::New();
    auto cellTypes = vtkSmartPointer<vtkIntArray>::New();
    auto points = vtkSmartPointer<vtkPoints>::New();

    while (ifs.is_open() && !ifs.fail()) {
        std::string line;
        std::getline(ifs, line);
        std::istringstream iss(line);
        switch (state) {
        case UNDEFINED: {
            if (line == "$MeshFormat") {
                state = MESH_FORMAT;
            } else if (line == "$Nodes") {
                state = NODES;
                std::getline(ifs, line);
                iss.str(line);
                std::size_t numNodes;
                iss >> numNodes;
                points->SetNumberOfPoints(numNodes);
            } else if (line == "$Elements") {
                state = ELEMENTS;
                std::getline(ifs, line);
                iss.str(line);
                std::size_t numElements;
                iss >> numElements;
                cells->Allocate(numElements);
                cellTypes->Allocate(numElements);
            }
            break;
        }
        case MESH_FORMAT: {
            if (line == "$EndMeshFormat")
                state = UNDEFINED;
            break;
        }
        case NODES: {
            if (line == "$EndNodes") {
                state = UNDEFINED;
            } else {
                std::size_t nodeNum;
                double x, y, z;
                iss >> nodeNum >> x >> y >> z;
                points->SetPoint(nodeNum - 1, x, y, z);
            }
            break;
        }
        case ELEMENTS: {
            if (line == "$EndElements") {
                state = UNDEFINED;
            } else {
                std::size_t elementNum;
                int elementType, numberOfTags;
                iss >> elementNum >> elementType >> numberOfTags;
                for (int i = 0; i < numberOfTags; ++i) { int dummy; iss >> dummy; } // drop tags
                auto it = typeMshToVtk.find(elementType);
                if (it != typeMshToVtk.cend()) {
                    elementType = std::get<0>(it->second);
                    std::size_t numNodes = std::get<1>(it->second);
                    auto idList = vtkSmartPointer<vtkIdList>::New();
                    idList->SetNumberOfIds(numNodes);
                    for (auto i = 0; i < numNodes; ++i) {
                        std::size_t nodeId;
                        iss >> nodeId;
                        idList->SetId(i, nodeId - 1);
                    }
                    cells->InsertNextCell(idList);
                    cellTypes->InsertNextValue(elementType);
                } else
                    std::cerr << "second, third and forth order elements are not supported (" << elementType << ")." << std::endl;
            }
        }
        }
    }

    points->Squeeze();
    cells->Squeeze();
    cellTypes->Squeeze();

    std::cout << "number of nodes " << points->GetNumberOfPoints() << std::endl;
    std::cout << "number of cells " << cells->GetNumberOfCells() << std::endl;
    std::cout << "number of cellTypes " << cellTypes->GetNumberOfTuples() << std::endl;

    auto mesh = vtkUnstructuredGrid::New();
    mesh->SetPoints(points);
    mesh->SetCells(cellTypes->GetPointer(0), cells);

    return mesh;
}

int main(int argc, char** argv) {
    if (argc != 2 && argc != 3) {
        std::cerr << "syntax: MSHToVTU <input.msh> [<output.vtu>]" << std::endl;
        return 1;
    }

    std::string input(argv[1]);
    std::string output;
    if (argc == 3)
        output = argv[2];
    else
        output = input.substr(0, input.find_last_of(".")) + ".vtu";

    std::cout << "converting from " << input << " to " << output << std::endl;

    auto mesh = vtkSmartPointer<vtkUnstructuredGrid>::Take(readMSH(input));
    auto writer = vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
    writer->SetFileName(output.c_str());
    writer->SetInputData(mesh);
    writer->Write();

    return 0;
}